
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AFNetworking
#define COCOAPODS_POD_AVAILABLE_AFNetworking
#define COCOAPODS_VERSION_MAJOR_AFNetworking 1
#define COCOAPODS_VERSION_MINOR_AFNetworking 3
#define COCOAPODS_VERSION_PATCH_AFNetworking 4

// Bolts/Tasks
#define COCOAPODS_POD_AVAILABLE_Bolts_Tasks
#define COCOAPODS_VERSION_MAJOR_Bolts_Tasks 1
#define COCOAPODS_VERSION_MINOR_Bolts_Tasks 1
#define COCOAPODS_VERSION_PATCH_Bolts_Tasks 5

// Google/Analytics
#define COCOAPODS_POD_AVAILABLE_Google_Analytics
#define COCOAPODS_VERSION_MAJOR_Google_Analytics 1
#define COCOAPODS_VERSION_MINOR_Google_Analytics 0
#define COCOAPODS_VERSION_PATCH_Google_Analytics 7

// Google/Core
#define COCOAPODS_POD_AVAILABLE_Google_Core
#define COCOAPODS_VERSION_MAJOR_Google_Core 1
#define COCOAPODS_VERSION_MINOR_Google_Core 0
#define COCOAPODS_VERSION_PATCH_Google_Core 7

// GoogleAnalytics
#define COCOAPODS_POD_AVAILABLE_GoogleAnalytics
#define COCOAPODS_VERSION_MAJOR_GoogleAnalytics 3
#define COCOAPODS_VERSION_MINOR_GoogleAnalytics 12
#define COCOAPODS_VERSION_PATCH_GoogleAnalytics 0

// GoogleNetworkingUtilities
#define COCOAPODS_POD_AVAILABLE_GoogleNetworkingUtilities
#define COCOAPODS_VERSION_MAJOR_GoogleNetworkingUtilities 1
#define COCOAPODS_VERSION_MINOR_GoogleNetworkingUtilities 0
#define COCOAPODS_VERSION_PATCH_GoogleNetworkingUtilities 0

// GoogleSymbolUtilities
#define COCOAPODS_POD_AVAILABLE_GoogleSymbolUtilities
#define COCOAPODS_VERSION_MAJOR_GoogleSymbolUtilities 1
#define COCOAPODS_VERSION_MINOR_GoogleSymbolUtilities 0
#define COCOAPODS_VERSION_PATCH_GoogleSymbolUtilities 0

// GoogleUtilities
#define COCOAPODS_POD_AVAILABLE_GoogleUtilities
#define COCOAPODS_VERSION_MAJOR_GoogleUtilities 1
#define COCOAPODS_VERSION_MINOR_GoogleUtilities 0
#define COCOAPODS_VERSION_PATCH_GoogleUtilities 1

// JSBadgeView
#define COCOAPODS_POD_AVAILABLE_JSBadgeView
#define COCOAPODS_VERSION_MAJOR_JSBadgeView 1
#define COCOAPODS_VERSION_MINOR_JSBadgeView 3
#define COCOAPODS_VERSION_PATCH_JSBadgeView 2

// Parse
#define COCOAPODS_POD_AVAILABLE_Parse
// This library does not follow semantic-versioning,
// so we were not able to define version macros.
// Please contact the author.
// Version: 1.7.2.1.

// SDWebImage
#define COCOAPODS_POD_AVAILABLE_SDWebImage
#define COCOAPODS_VERSION_MAJOR_SDWebImage 3
#define COCOAPODS_VERSION_MINOR_SDWebImage 7
#define COCOAPODS_VERSION_PATCH_SDWebImage 2

// SDWebImage/Core
#define COCOAPODS_POD_AVAILABLE_SDWebImage_Core
#define COCOAPODS_VERSION_MAJOR_SDWebImage_Core 3
#define COCOAPODS_VERSION_MINOR_SDWebImage_Core 7
#define COCOAPODS_VERSION_PATCH_SDWebImage_Core 2

// SVProgressHUD
#define COCOAPODS_POD_AVAILABLE_SVProgressHUD
#define COCOAPODS_VERSION_MAJOR_SVProgressHUD 1
#define COCOAPODS_VERSION_MINOR_SVProgressHUD 1
#define COCOAPODS_VERSION_PATCH_SVProgressHUD 2

// ZXingObjC
#define COCOAPODS_POD_AVAILABLE_ZXingObjC
#define COCOAPODS_VERSION_MAJOR_ZXingObjC 3
#define COCOAPODS_VERSION_MINOR_ZXingObjC 1
#define COCOAPODS_VERSION_PATCH_ZXingObjC 0

// ZXingObjC/All
#define COCOAPODS_POD_AVAILABLE_ZXingObjC_All
#define COCOAPODS_VERSION_MAJOR_ZXingObjC_All 3
#define COCOAPODS_VERSION_MINOR_ZXingObjC_All 1
#define COCOAPODS_VERSION_PATCH_ZXingObjC_All 0

